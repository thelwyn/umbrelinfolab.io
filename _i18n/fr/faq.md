# Questions fréquemment posées
{: .no_toc }

Questions générales et autres informations concernant la technologie utilisée dans Umbrel.

Les problèmes relatifs aux opérations de base de votre noeud Umbrel sont addressés dans le guide séparé [Résolution de problèmes](troubleshooting.md).

## Table des matières
{: .no_toc .text-delta }

1. Table des matières
{:toc}

### Est-ce que je peux devenir riche en acheminant des paiements Lightning?

Personne ne peut le dire. Probablement pas. Vous gagnerez de petites commissions. Je m'en fiche. Eclatez-vous!

### Est-ce que je peux connecter mon disque dur formaté en ext4 à mon PC?

Le système de fichiers ext4 n'est pas compatible de base avec Windows, mais avec des logiciels additionnels comme [Linux File Systems](https://www.paragon-software.com/home/linuxfs-windows/#faq) de Paragon Software (ils offrent 10 jours d'essai gratuit), c'est possible.

### Est-ce que je peux utiliser une blockchain déjà téléchargée auparavant pour gagner du temps lors de l'établissement de mon noeud Umbrel?

Ce n'est pas pris en charge nativement par Umbrel pour différentes raisons mais c'est faisable -à vos risques et périls!- en suivant ces instructions: [Utiliser une blockchain déjà téléchargée](https://github.com/getumbrel/umbrel-os/issues/119).

### Qu'est-ce que font toutes ces commandes Linux?

Voici une (très) courte liste des commandes Linux les plus courantes. Pour une commande en particulier, vous pouvez taper `man [commande]` pour afficher la page de manuel correspondante (taper `q` pour sortir).

| Commande     | Description                                           | Exemple                                      |
| ------------ | ----------------------------------------------------- | -------------------------------------------- |
| `cd`         | changer de dossier                                    | `cd /home/umbrel`                            |
| `ls`         | lister le contenu du dossier                          | `ls -la /home/umbrel/umbrel`                 |
| `cp`         | copier                                                | `cp file.txt newfile.txt`                    |
| `mv`         | déplacer                                              | `mv file.txt moved_file.txt`                 |
| `rm`         | effacer                                               | `rm temporaryfile.txt`                       |
| `mkdir`      | créer un dossier                                      | `mkdir /home/umbrel/newdirectory`            |
| `ln`         | créer un lien                                         | `ln -s /target_directory /link`              |
| `sudo`       | executer une commande en tant que superutilisateur    | `sudo nano textfile.txt`                     |
| `su`         | changer de compte utilisateur                         | `sudo su root`                               |
| `chown`      | changer le propriétaire du fichier                    | `chown umbrel:umbrel myfile.txt`             |
| `chmod`      | changer les permissions du fichier                    | `chmod +x executable.script`                 |
| `nano`       | éditeur de texte                                      | `nano textfile.txt`                          |
| `tar`        | outil d'archivage                                     | `tar -cvf archive.tar file1.txt file2.txt`   |
| `exit`       | sortir de sa session utilisateur                      | `exit`                                       |
| `systemctl`  | controler le service systemd                          | `sudo systemctl start umbrel-startup`        |
| `journalctl` | interroger le journal systemd                         | `sudo journalctl -u umbrel-external-storage` |
| `htop`       | superviser les processus et les ressources sutilisées | `htop`                                       |
| `shutdown`   | éteindre ou redémarrer le Pi                          | `sudo shutdown -r now`                       |

### Où puis-je obtenir plus d'informations?

Si vous voulez en apprendre plus sur Bitcoin et que vous êtes curieux à propos de fonctionnement interne du réseau Lightning, les articles suivants du Bitcoin Magazine offrent une très bonne introduction:

- [What is Bitcoin?](https://bitcoinmagazine.com/guides/what-bitcoin)
- [Understanding the Lightning Network](https://bitcoinmagazine.com/articles/understanding-the-lightning-network-part-building-a-bidirectional-payment-channel-1464710791/)
- [Bitcoin resources](https://www.lopp.net/bitcoin-information.html) and [Lightning Network resources](https://www.lopp.net/lightning-information.html) by Jameson Lopp

### Est-ce que Umbrel supporte …?

Pas pour le moment, mais Umbrel a une infrastructure en applications, et des dévelopeurs tierces peuvent donc ajouter des applications à Umbrel et les publier dans son [App Store](https://medium.com/getumbrel/introducing-the-umbrel-app-store-7a2068c64a10).

---

Ce guide sur les questions les plus fréquentes sera continuellement mis à jour à mesure que les problèmes se présenteront. Les contributions sont les bienvenues via un merge request.

---
