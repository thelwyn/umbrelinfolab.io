# Troubleshooting
{: .no_toc }

Fragen zu Umbrel und der Verwendung von Umbrel.

Allgemeine Fragen zu Bitcoin & Lightning werden in den [FAQ](faq.md) beantwortet.

## Inhaltzverzeichnis
{: .no_toc .text-delta }

1. TOC
{:toc}

### Kann ich mich per SSH verbinden?

Ja! Öffne ein Terminal auf deinem Computer und gib `ssh umbrel@umbrel.local` ein, das Passwort ist `moneyprintergobrrr`.
Ab Version 0.3.3 ist das Passwort stattdessen dein persönliches Nutzerpasswort.

### Ist ein Raspberry Pi kompatibel?

Umbrel läuft auf jedem Raspberry Pi mit 4GB RAM.

### Mein Umbrel Node startet nicht. Was kann ich tun?

Hast du irgendwas (Lüfter, etc.) mit den GPIO-Pins verbunden? Wenn ja, versuche mal, Umbrel ohne das zu starten.

### Mein Node ist nicht auf Umbrel.local erreichbar oder stürzt immer wieder ab. Was kann ich tun?

Überprüfe, ob dein Router das Node erkennt.
Wenn nicht, startet das Node nicht oder das LAN-Kabel ist nicht korrekt verbunden/das WLAN ist nicht korrekt eingerichtet.
Wenn du denkst, das das Node nicht startet, schau bei der vorherigen Frage.

Wenn das Node erkannt wird, versuche es direkt mit der IP-Adresse zu erreichen.
Wenn das auch nicht hilft, versuche, das Problem [über SSH](#can-i-login-using-ssh) zu finden:

```
~/umbrel/scripts/debug --upload
```

Du wirst automatisch über den nächsten Schritt informiert.
Statt im ofiziellen Telegram-Server kann dir [auch auf Deutsch geholfen werden](https://t.me/umbrelgerman).

### Ich möchte mich über mein locales Netzwerk mit Umbrel verbinden, aber es geht nicht. Wie kann ich das beheben?

Dazu musst du einfach in der Verbuindungsadresse die onion-URL mit umbrel.local ersetzen.

### Eine statische IP-Adresse festlegen

Wenn dein Router es nicht direkt erlaubt, eine statische IP festzulegen, dann geht das auch so für dein Node,

Dazu muss der DHCP-Client (auf dem Pi) so konfiguriert sein, dass er eine statische IP-Adresse an den DHCP-Server (meistens der Router) sendet, bevor dieser dem Pi eine andere IP-Adresse gibt.

Die folgenden Befehle sollten über SSH ausgeführt werden

1. Bekomme die Gateway-Adresse
   Gib `netstat -r -n` ein und wähle die IP-Adresse aus der Gateway-Spalte, die nicht `0.0.0.0` ist. In meinem Fall ist das `192.168.178.1`.

2. Konfiguriere die IP-Adresse, den DNS-Server und das Gateway auf dem Pi
   Die Konfiguration für den DHCP-Client auf dem Pi ist in der Datei `/etc/dhcpcd.conf` gespeichert:

   ```
   sudo nano /etc/dhcpcd.conf
   ```

   Der folgende Code ist ein Beispiel für eine Konfiguration. Ändere den Wert von `static routers` und `static domain_name_servers` auf. Achte daruaf, dem Pi eine IP-Adresse **AUSERHALB** des Adressbereiches der Adressen, die der DHCP-Server gibt, festzulegen. Dieser Adressbereich sollte auf der Einstellungsseite deines Routers zu finden sein. Wenn der DHCP-Adressbereich `192.168.178.1` bis `192.168.178.99` ist,  dann geht `192.168.178.100` als IP für den Pi.

   Füge das zu `/etc/dhcpcd.conf` hinzu:

   ```
   # Configuration static IP address (CHANGE THE VALUES TO FIT FOR YOUR NETWORK)
   interface eth0
   static ip_address=192.168.178.100/24
   static routers=192.168.178.1
   static domain_name_servers=192.168.178.1
   ```

3. Starte das Netzwerksystem neu:
   `sudo /etc/init.d/networking restart`

### WiFi statt Ethernet verwenden

- Erstelle eine Datei `wpa_supplicant.conf` auf der Bootpartition deiner SD-Karte mit nachfolgendem Inhalt.
  Beachte, dass der Netzwerkname und das Passwort in Anführungszeichen sein müssen (z.B. `psk="password"`).

  ```conf
  ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
  update_config=1
  country=[COUNTRY_CODE]
  network={
    ssid="[WIFI_SSID]"
    psk="[WIFI_PASSWORD]"
  }
  ```

- Ersetze `[COUNTRY_CODE]` mit dem [ISO2 code](https://www.iso.org/obp/ui/#search){:target="\_blank"} deines Landes (z.B. `DE`)
- Ersetze `[WIFI_SSID]` und `[WIFI_PASSWORD]`mit den entsprechenden Zugangsdaten deines eigenen WLANs.

### Manueller Zugriff auf `bitcon-cli` und `lncli`

Diese beiden Tools sind auf Umbrel immer in UMBREL_ROOT_DIR/bin/. Auf Umbrel OS kannst du sie so [über SSH verwenden](#kann-ich-mich-per-ssh-verbinden):

```
~/umbrel/bin/bitcoin-cli
```

und

```
~/umbrel/bin/lncli
```

### Umbrel komplett zurücksetzen (Wenn du dein Passwort vergessen hast)

Mach dass nur,  **wenn du kein Geld** auf LND hast! Ansonsten speichere den Seed und die Backupdatei, damit du dein Geld später wiederherstellen kannst

Der Seed, alle Apps, Einstellungen und Daten werden dabei gelöscht!

```
sudo systemctl stop umbrel-startup && sudo rm -rf ~/umbrel/lnd/!(lnd.conf) && sudo rm ~/umbrel/db/user.json && sudo rm ~/umbrel/db/umbrel-seed/seed && sudo systemctl start umbrel-startup
```

### Manuell ein Update installieren

Führe das für ein manuelles Update [über SSH aus](#kann-ich-mich-per-ssh-verbinden):

```
cd ~/umbrel && sudo ./scripts/update/update --repo getumbrel/umbrel#v0.3.5
```

Ersetze v0.3.5 mit der Version, auf die du updaten willst.

Wenn das Update feststeckt, kann das vieleicht helfen:

```
sudo rm statuses/update-in-progress 
```

### Von einer channels.backup wiederherstellen (Umbrel OS)

Nach dem Wiederherstellen von den 24 Worten kann es einige Minuten, aber auch Stunden dauern, bis alle Bitcoin (on-chain) Transaktionen wiederhergestellt sind
Währenddessen (aber auch danach) kannst du folgendermaßen die aus einer channels.backup dein Geld aus den Lightning-Channels holen.

#### Schritt 1: Die Datei auf das Node kopieren

Öffne ein Terminal auf deinem Computer und gib das ein:

```
scp <pfad/deiner/channel/backup/datei> umbrel@umbrel.local:/home/umbrel/umbrel/lnd/channel.backup
```

_(Ersetze `<pfad/deiner/channel/backup/datei>` durch den exakten Pfad deiner channels.backup-Datei auf deinem Computer.)_

Das Passwort ist `moneyprintergobrrr`, ab Version 0.3.3 ist das Passwort stattdessen dein persönliches Nutzerpasswort.

####  Schritt 2: Verbinde dich per SSH

[Eine Anleitung dazu findest du hier.](#kann-ich-mich-per-ssh-verbinden)

#### Schritt 3: Channel wiederherstellen

```
cd ~/umbrel && ./bin/scripts/lncli restorechanbackup --multi_file /data/.lnd/channel.backup
```

Warte eine Minute, nachdem du das ausgeführt hast. Du solltest nun auf http://umbrel.local/lightning sehen, dass die Channels geschlossen werden.

---

Diese Anleitung wird ständig mit neu gefundenen Tipps oder welchen, die als Issue gemeldet werden, aktualisiert. Mit einem Merge Request kannst du jederzeit mitmachen!

---
