# Häufig gestellte Fragen
{: .no_toc }

Generelle Fragen und weitere Informationen zu der in Umbrel eingesetzten Technologie.

Fragen zu Umbrel und der Verwendung von Umbrel werden auf der [Troubleshooting-Seite](troubleshooting.md) beantwortet.

## Inhaltsverzeichnis
{: .no_toc .text-delta }

1. TOC
{:toc}

### Kann ich durch Lightning-Channel reich werden?

Das weiß niemand. Wahrscheinlich nicht. Du wirst geringste Gebühren bekommen. Ist egal. Viel Spaß bei Umbrel!

### Kann ich die externe SSD mit meinem Windows-Computer lesen?

Ext4 kann auf Windows nur mit externer Software wie [Linux File Systems](https://www.paragon-software.com/home/linuxfs-windows/#faq) von Paragon Software (10-Tage gratis Testen) verwendet werden.

### Can I use previously downloaded blockchain to save time when setting up my Umbrel node?

This is not natively supported by Umbrel for different reasons but you can make it at your own risk following these instructions:  [Use previously downloaded blockchain](https://github.com/getumbrel/umbrel-os/issues/119).

### Was machen die ganzen Linux-Befehle?

Dies ist eine (sehr) kurze Liste von häufigen Linux-Befehlen. Für einen Befehl liefert `man [Befehl]` (ohne die eckigen Klammern) mehr Informationen (`q` zum Beenden eingeben).

| Befehl       | Beschreibung                               | Beispiel                                     |
| ------------ | ------------------------------------------ | -------------------------------------------- |
| `cd`         | In einen Ordner wechseln                   | `cd /home/umbrel`                            |
| `ls`         | Ordnerinhalt auflisten                     | `ls -la /home/umbrel/umbrel`                 |
| `cp`         | Kopieren                                   | `cp file.txt newfile.txt`                    |
| `mv`         | Verschieben                                | `mv file.txt moved_file.txt`                 |
| `rm`         | Löschen                                    | `rm temporaryfile.txt`                       |
| `mkdir`      | Ordner erstellen                           | `mkdir /home/umbrel/newdirectory`            |
| `ln`         | Verknüpfung erstellen                      | `ln -s /target_directory /link`              |
| `sudo`       | Befehl as Administrator ausführen          | `sudo nano textfile.txt`                     |
| `su`         | Account wechseln                           | `sudo su root`                               |
| `chown`      | Dateieigentümer ändern                     | `chown umbrel:umbrel myfile.txt`             |
| `chmod`      | Dateiberechtigungen ändern                 | `chmod +x executable.script`                 |
| `nano`       | Texteditor                                 | `nano textfile.txt`                          |
| `tar`        | Archivtool                                 | `tar -cvf archive.tar file1.txt file2.txt`   |
| `exit`       | Aktuelle Sitzung beenden                   | `exit`                                       |
| `systemctl`  | Systemd-Services verwalten                 | `sudo systemctl start umbrel-startup`        |
| `journalctl` | Systemlogs anzeigen                        | `sudo journalctl -u umbrel-external-storage` |
| `htop`       | Prozesse & Ressourcennutzung beobachten    | `htop`                                       |
| `shutdown`   | Den Pi Herunterfahren oder neu starten     | `sudo shutdown -r now`                       |

### Wo gibts mehr Informationen?

Die folgenden Artikel liefern mehr Informationen zu Bitcoin & Lightning:

- [What is Bitcoin?](https://bitcoinmagazine.com/guides/what-bitcoin)
- [Understanding the Lightning Network](https://bitcoinmagazine.com/articles/understanding-the-lightning-network-part-building-a-bidirectional-payment-channel-1464710791/)
- [Bitcoin resources](https://www.lopp.net/bitcoin-information.html) und [Lightning Network resources](https://www.lopp.net/lightning-information.html) von Jameson Lopp

Auf Deutsch gibt's auch Videos vom [Blocktrainer](https://blocktrainer.de).

### Unterstützt Umbrel …?

Aktuell nicht, aber externe Entwickler können es vielleicht im [App Store](https://medium.com/getumbrel/introducing-the-umbrel-app-store-7a2068c64a10) veröffentlichen.

---

Diese Seite  oft mit neuen Entdeckungen aktualisiert. Mach doch mit einem Merge Request mit!

---
